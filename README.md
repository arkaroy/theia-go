# Go Development Environment with Theia

Run on http://localhost:3000 with the current directory as a workspace:
```
docker run -d -it -v $(pwd):/home/project -p 3000:3000 -u $(id -u):$(id -g) --name theia-go --init iarkaroy/theia-go
```

The `-u` flag will map the current user to `theia` user in container allowing access to `/home/theia`, `/home/project` and `/home/go` (set as `GOPATH`).

In container `GO111MODULE` is set to `on`.

To forward network interfaces of host, use `–net=host` flag.