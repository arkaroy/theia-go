ARG NODE_VERSION=12.22.1
FROM node:$NODE_VERSION-alpine as theia
WORKDIR /home/theia
COPY package.json ./package.json
RUN apk add --no-cache --update make pkgconfig gcc g++ python3 libx11-dev libxkbfile-dev
RUN yarn --pure-lockfile && \
    NODE_OPTIONS="--max_old_space_size=4096" yarn theia build && \
    yarn theia download:plugins && \
    yarn --production && \
    yarn autoclean --init && \
    echo *.ts >> .yarnclean && \
    echo *.ts.map >> .yarnclean && \
    echo *.spec.* >> .yarnclean && \
    yarn autoclean --force && \
    yarn cache clean

FROM alpine:latest
# RUN addgroup theia && \
#     adduser -G theia -s /bin/sh -D theia;
# RUN chmod g+rw /home && \
#     mkdir -p /home/project && \
#     mkdir -p /home/go && \
#     chown -R theia:theia /home/theia && \
#     chown -R theia:theia /home/project && \
#     chown -R theia:theia /home/go;
RUN mkdir -p /theia && \
    mkdir -p /project && \
    mkdir -p /go && \
    mkdir -p /home
RUN apk add --no-cache --update git openssh-client openssl bash curl gcc g++ pkgconfig make fontconfig \
    php7 php7-bcmath php7-bz2 php7-common php7-ctype php7-curl php7-gd php7-gettext php7-gmp php7-iconv php7-imap php7-intl php7-json php7-mbstring php7-mysqli composer \
    && curl -SLO "https://unofficial-builds.nodejs.org/download/release/v12.22.1/node-v12.22.1-linux-x64-musl.tar.xz" \
    && tar -xJf "node-v12.22.1-linux-x64-musl.tar.xz" -C /usr/local --strip-components=1 --no-same-owner \
    && rm "node-v12.22.1-linux-x64-musl.tar.xz" \
    && ln -s /usr/local/bin/node /usr/local/bin/nodejs
COPY --from=theia --chown=root:root /home/theia /theia
COPY --chown=root:root ./settings.json /theia/.theia/settings.json
COPY --chown=root:root ./install-firacode.sh /theia/install-firacode.sh
COPY --from=golang:alpine /usr/local/go/ /usr/local/go/
WORKDIR /theia
ENV GOOS=linux \
    GOARCH=amd64 \
    GOPATH=/go \
    GO111MODULE=on \
    PATH=$GOPATH/bin:/usr/local/go/bin:$PATH \
    SHELL=/bin/sh \
    THEIA_DEFAULT_PLUGINS=local-dir:/theia/plugins \
    USE_LOCAL_GIT=true
RUN mkdir -p /root/.ssh \
    && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > /root/.ssh/config
USER root
RUN go get golang.org/x/tools/gopls && \
    go get -u -v github.com/uudashr/gopkgs/v2/cmd/gopkgs && \
    go get -u -v golang.org/x/lint/golint && \
    go get -u -v github.com/ramya-rao-a/go-outline && \
    go get -u -v github.com/go-delve/delve/cmd/dlv && \
    go get -u -v honnef.co/go/tools/cmd/staticcheck && \
    go get -u -v github.com/fatih/gomodifytags && \
    go get -u -v github.com/josharian/impl && \
    go get -u -v github.com/cweill/gotests/... && \
    go get -u -v github.com/haya14busa/goplay/cmd/goplay
RUN chmod +x ./install-firacode.sh && \
    ./install-firacode.sh && \
    rm install-firacode.sh
EXPOSE 3000
ENTRYPOINT [ "node", "/theia/src-gen/backend/main.js", "/project", "--hostname=0.0.0.0" ]
